import {
    checkResponseTime,
    checkStatusCode,
} from "../../helpers/functionsForChecking.helper";
import { AuthController } from "../lib/controllers/auth.controller";
import { PostsController } from "../lib/controllers/posts.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { USER_DATA } from "../shared/constants";
import { IUserDataSet } from "../shared/models/user.interface";

const auth = new AuthController();
const post = new PostsController();
const users = new UsersController();

describe("Negative tests", () => {
    let accessToken: string;

    const invalidCredentialsDataSet: IUserDataSet[] = [
        { email: "dima1223@test.com", password: "qwerty" },
        { email: "dima@testcom", password: "qwerty" },
        { email: "dima@test@@com", password: "qwerty" },
        { email: "dima   @test.com", password: "qwerty" },
        { email: "dima@test.  com", password: "qwerty" },
        { email: "dimatest.com", password: "qwerty" },
    ];

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`Login with invalid user credentials email: '${credentials.email}', password: '${credentials.password}'`, async () => {
            let response = await auth.login(
                credentials.email,
                credentials.password
            );

            checkStatusCode(response, 404);
            checkResponseTime(response, 3000);
        });
    });

    it(`Add like to the post with invalid post and user id's: entityId:7777777, userId: 89898989`, async () => {
        const likeBody = {
            entityId: 7777777,
            userId: 89898989,
            isLike: true,
        };

        let authResponse = await auth.login(
            USER_DATA.email,
            USER_DATA.password
        );
        accessToken = authResponse.body.token.accessToken.token;

        const response = await post.addLikeToPost(likeBody, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
    });

    it(`Update user data with invalid userId: id:5555555`, async () => {
        const response = await users.updateUser(
            { id: 5555555, userName: "dima-qq" },
            accessToken
        );

        checkStatusCode(response, 404);
        checkResponseTime(response, 3000);
    });
});
