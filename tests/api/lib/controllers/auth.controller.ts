import { ApiRequest } from "../request";
import { API_URL, POST } from "../../shared/constants";

export class AuthController {
    async login(emailValue: string, passwordValue: string) {
        const response = await new ApiRequest()
            .prefixUrl(API_URL)
            .method(POST)
            .url(`api/Auth/login`)
            .body({
                email: emailValue,
                password: passwordValue,
            })
            .send();
        return response;
    }
}
