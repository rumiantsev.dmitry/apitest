import { ApiRequest } from "../request";
import { API_URL, POST } from "../../shared/constants";
import { IRegisterBody } from "../../shared/models/register.interface";

export class RegisterController {
    async register(body: IRegisterBody) {
        const { avatar, email, userName, password } = body;

        const response = await new ApiRequest()
            .prefixUrl(API_URL)
            .method(POST)
            .url(`api/Register`)
            .body({
                avatar,
                email,
                userName,
                password,
            })
            .send();
        return response;
    }
}
