import { IRegisterBody } from "./models/register.interface";

// Base API url
export const API_URL: string = global.appConfig.baseUrl;

// API methods
export const GET = "GET";
export const POST = "POST";
export const PUT = "PUT";
export const DELETE = "DELETE";

// User Data
export const USER_DATA: IRegisterBody = {
    avatar: "",
    email: "dima@test.com",
    userName: "Dima",
    password: "qwerty",
};
