export interface IUser {
    id: number;
    avatar: string;
    email: string;
    userName: string;
}

export interface IUserDataSet {
    email: string;
    password: string;
}
