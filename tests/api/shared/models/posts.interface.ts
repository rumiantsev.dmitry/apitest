export interface IPostData {
    authorId: number;
    previewImage: string;
    body: string;
}

export interface ILikeData {
    entityId: number;
    isLike: boolean;
    userId: number;
}
